package com.cgi.bootcamp.jg.servicenow.controller;

import com.cgi.bootcamp.jg.servicenow.api.CreateTicketDto;
import com.cgi.bootcamp.jg.servicenow.api.PageDto;
import com.cgi.bootcamp.jg.servicenow.api.TicketDto;
import com.cgi.bootcamp.jg.servicenow.service.TicketService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * REST Ticket controller.
 * Calls TicketService.
 * Enables viewing tickets by id or name and
 * creation of a new ticket.
 * Also enables viewing all records page by page.
 */
@RestController
@RequestMapping("/tickets")
@Api(value = "Ticket Management System")
public class TicketController {

    private TicketService ticketService;

    @Autowired
    public TicketController(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    /**
     * Shows ticket by id
     * @param id Id of ticket to view
     * @return TicketDto
     */
    @ApiOperation(value = "View ticket by id", response = TicketDto.class)
    @GetMapping("/{id}")
    public TicketDto getTicketById(@ApiParam(value = "Id of ticket to retrieve", required = true)
                                   @PathVariable("id") Long id) {
        return ticketService.getTicketById(id);
    }

    /**
     * Shows ticket by name property
     * @param name Name of ticket to view
     * @return TicketDto
     */
    @ApiOperation(value = "View ticket by name", response = TicketDto.class)
    @GetMapping()
    public TicketDto getTicketByName(@ApiParam(value = "Name of ticket to retrieve", required = true)
                                     @RequestParam(name = "name") String name) {
        return ticketService.getTicketByName(name);
    }

    /**
     * Shows all tickets page by page
     * @param page Number of page to display
     * @param size Number of tickets per page
     * @return PageDto
     */
    @ApiOperation(value = "View all tickets page by page, default pagesize is 20", response = PageDto.class)
    @GetMapping("/all")
    public PageDto getAllTickets(@ApiParam(value = "Page number", required = true)
                                 @RequestParam(name = "page") Long page,
                                 @ApiParam(value = "Items per page")
                                 @RequestParam(name = "size", defaultValue = "20") Long size) {
        return ticketService.getTicketsPage(page, size);
    }

    /**
     * Add new ticket to the system
     * @param newTicket Properties of ticket to add, name should be unique
     */
    @ApiOperation(value = "Create new ticket")
    @PostMapping()
    public void postTicket(@ApiParam(value = "New ticket data", required = true)
                           @RequestBody CreateTicketDto newTicket) {
        ticketService.saveTicket(newTicket);
    }

}
