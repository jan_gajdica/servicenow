package com.cgi.bootcamp.jg.servicenow.config;

import org.apache.activemq.artemis.jms.client.ActiveMQQueue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import javax.jms.Destination;

@Configuration
@EnableJms
public class MessagingConfig {

    @Bean
    public Destination ticketsQueue() {
        return new ActiveMQQueue("servicenow.tickets.queue");
    }
}
