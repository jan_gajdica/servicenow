package com.cgi.bootcamp.jg.servicenow;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Entry point to Spring Boot application
 */
@SpringBootApplication
public class ServicenowApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServicenowApplication.class, args);
    }

}
