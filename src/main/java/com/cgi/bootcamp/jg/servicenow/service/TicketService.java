package com.cgi.bootcamp.jg.servicenow.service;

import com.cgi.bootcamp.jg.servicenow.api.CreateTicketDto;
import com.cgi.bootcamp.jg.servicenow.api.PageDto;
import com.cgi.bootcamp.jg.servicenow.api.TicketDto;
import com.cgi.bootcamp.jg.servicenow.messaging.JmsProducerService;
import com.cgi.bootcamp.jg.servicenow.persistence.entities.Ticket;
import com.cgi.bootcamp.jg.servicenow.persistence.repositories.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

/**
 * Provides operations over tickets.
 * Calls TicketRepository.
 * Calls JmsProducerService
 * Translates between DTO and entity classes.
 */
@Service
public class TicketService {

    private TicketRepository ticketRepository;
    private JmsProducerService jmsProducerService;

    @Autowired
    public TicketService(TicketRepository ticketRepository, JmsProducerService jmsProducerService) {
        this.ticketRepository = ticketRepository;
        this.jmsProducerService = jmsProducerService;
    }

    /**
     * Returns ticket by id.
     * @param id Id of ticket to be returned
     * @return TicketDto
     */
    public TicketDto getTicketById(Long id) {
        return mapTicketToDto(ticketRepository.getTicketById(id));
    }

    /**
     * Returns ticket by name.
     * @param name Unique name of ticket to return
     * @return TicketDto
     */
    public TicketDto getTicketByName(String name) {
        return mapTicketToDto(ticketRepository.getTicketByName(name));
    }

    /**
     * Construcs PageDto objects.
     * @param page Nuber of page counting from 1
     * @param size Number of tickets per page
     * @return PageDto
     */
    public PageDto getTicketsPage(Long page, Long size) {
        Long offset = size * (page -1);
        List<Ticket> tickets = ticketRepository.getTickets(offset, size);
        List<TicketDto> dtoList = new ArrayList<>();
        tickets.forEach(ticket -> dtoList.add(mapTicketToDto(ticket)));
        Long ticketsCount = ticketRepository.countTickets();
        PageDto pageDto = new PageDto();
        pageDto.setTickets(dtoList);
        pageDto.setAllTickets(ticketsCount);
        return pageDto;
    }

    /**
     * Adds new ticket to system.
     * Send the new ticket to messaging service
     * @param ticket New ticket to be stored
     */
    public void saveTicket(CreateTicketDto ticket) {
        jmsProducerService.send(ticket);
        ticketRepository.saveTicket(mapCreateTicketDtoToTicket(ticket));
    }

    /**
     * Converts Ticket entity object into TicketDto.
     * @param ticket To be converted
     * @return TicketDto
     */
    public static TicketDto mapTicketToDto(Ticket ticket) {
        TicketDto ticketDto = new TicketDto();
        ticketDto.setId(ticket.getId());
        ticketDto.setName(ticket.getName());
        ticketDto.setEmail(ticket.getEmail());
        ticketDto.setIdPersonCreator(ticket.getIdPersonCreator());
        ticketDto.setIdPersonAssigned(ticket.getIdPersonAssigned());
        ticketDto.setCreationDatetime(ticket.getCreationDatetime());
        return ticketDto;
    }

    /**
     * Converts CreateTicketDto object into Ticket entity.
     * @param newTicket To be converted
     * @return Ticket
     */
    public static Ticket mapCreateTicketDtoToTicket(CreateTicketDto newTicket) {
        Ticket ticket = new Ticket();
        ticket.setName(newTicket.getName());
        ticket.setEmail(newTicket.getEmail());
        ticket.setIdPersonCreator(newTicket.getIdPersonCreator());
        ticket.setIdPersonAssigned(newTicket.getIdPersonAssigned());
        ticket.setCreationDatetime(newTicket.getCreationDatetime());
        return ticket;
    }
}