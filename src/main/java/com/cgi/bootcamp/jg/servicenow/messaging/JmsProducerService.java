package com.cgi.bootcamp.jg.servicenow.messaging;

import com.cgi.bootcamp.jg.servicenow.api.CreateTicketDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import javax.jms.Destination;

@Service
public class JmsProducerService {

    private JmsTemplate jms;
    private Destination destination;

    @Autowired
    public JmsProducerService(JmsTemplate jms, Destination destination) {
        this.jms = jms;
        this.destination = destination;
    }

    public void send(CreateTicketDto newTicket) {
        jms.convertAndSend(destination, newTicket);
    }

}
