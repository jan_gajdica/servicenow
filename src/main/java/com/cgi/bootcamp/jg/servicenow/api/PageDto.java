package com.cgi.bootcamp.jg.servicenow.api;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 * Represents one page of tickets in paginated view.
 * Output DTO.
 */
@ApiModel(description = "A page of tickets")
public class PageDto {

    private List<TicketDto> tickets;
    @ApiModelProperty(value = "Number of all entries")
    private Long allTickets;

    public PageDto() {}

    public List<TicketDto> getTickets() {
        return tickets;
    }

    public void setTickets(List<TicketDto> tickets) {
        this.tickets = tickets;
    }

    public Long getAllTickets() {
        return allTickets;
    }

    public void setAllTickets(Long allTickets) {
        this.allTickets = allTickets;
    }

    @Override
    public String toString() {
        return "PageDto{" +
                "tickets=" + tickets +
                ", count=" + allTickets +
                '}';
    }
}
