package com.cgi.bootcamp.jg.servicenow.persistence.mappers;

import com.cgi.bootcamp.jg.servicenow.persistence.entities.Ticket;
import org.springframework.jdbc.core.RowMapper;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Describes how ticket object is created from database entry.
 */
public class TicketMapper implements RowMapper<Ticket> {

    @Override
    public Ticket mapRow(ResultSet resultSet, int i) throws SQLException {
        Ticket ticket = new Ticket();
        ticket.setId(resultSet.getLong("id"));
        ticket.setName(resultSet.getString("name"));
        ticket.setEmail(resultSet.getString("email"));
        ticket.setIdPersonCreator(resultSet.getLong("id_person_creator"));
        ticket.setIdPersonAssigned(resultSet.getLong("id_person_assigned"));
        ticket.setCreationDatetime(resultSet.getString("creation_datetime"));
        return ticket;
    }
}
